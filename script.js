/*Referência de botões de aposta
    comando -> $('.actions button:nth-child(n)').click()
  onde 'n' é a posição do botão
    n   |   Botão  |
    1   |    clear |
    2   |    +0.01 |
    3   |    +0.10 |
    4   |    +1.00 |
    5   |   +10.00 |
    6   |  +100.00 |
    7   | +1000.00 |
    8   |    1/2   |
    9   |    2x    |
   10   |   Max    |
*/

var qtdMaxErros = 1000; //Quantidade máxima de erros
var apostaInicial = "ct";
var aposta        = apostaInicial;
var apostaDnv   = true;
var contErros   = 0;
var valorApostaInicial = 0.01;
var horaDeApostar;
var balanca;
var balancaRodadaPassada = parseFloat($('#balance').text().replace(",","."));
var primeiraAposta = true;
var interval;

function determinaValorAposta() {
  balanca = parseFloat($('#balance').text().replace(",","."));  //total antes da rodada

  if(balanca < balancaRodadaPassada) {
    contErros++;

    if(contErros > 2){
        if(aposta == "ct") {
          aposta = "tr";
        }else if(aposta == "tr") {
          aposta = "ct";
        }
    }

    balancaRodadaPassada = balanca;
    apostaMinima(contErros);
  }else if(balanca > balancaRodadaPassada) {
    contErros = 0;
    aposta = apostaInicial;
    balancaRodadaPassada = balanca;
    apostaMinima(contErros);
  }else if(balanca == balancaRodadaPassada) {
    if(primeiraAposta) {
      contErros   = 0;
      balancaRodadaPassada = balanca;
      apostaMinima(contErros);
      primeiraAposta = false;
    }else{
      if(confirm('Infelizmente a próxima aposta é maior que sua balança :(')){
        window.location.reload();
      }
    }
  }

}

function apagaAposta(){
  $(".actions button:nth-child(1)").click();
}

function clicksApostaMinima(numBotao, numVezes){
  if(!numVezes) return;
  for (var c = 1; c <= numVezes; c++) {
    $(".actions button:nth-child(" + numBotao + ")").click();
  }
}

function apostaMinima(erros){
  apagaAposta();
  var clickInteiro = Math.floor(valorApostaInicial/1);
  var clickDecimal = Math.floor((valorApostaInicial - clickInteiro)*10);
  var clickCent    = (((valorApostaInicial - clickInteiro)*10) - clickDecimal)*10;
  clicksApostaMinima(4, clickInteiro);
  clicksApostaMinima(3, clickDecimal);
  clicksApostaMinima(2, clickCent);
  clicksApostaMinima(9, erros);
}

function cliqueAposta() {
  if(apostaDnv) {
    determinaValorAposta();

    switch(aposta) {
      case "ct":
        $('.bet-option-heading:first').click()
        break;
      case "tr":
        $('.bet-option-heading:last').click();
        break;
    }

  }else{
    return;
  }

  apostaDnv = false;
}

interval = setInterval(function() { // Update every 2 seconds
  horaDeApostar = $('.rolling-indicator').css('display');

    if(horaDeApostar == 'none') {
      if(contErros <= qtdMaxErros) {
        cliqueAposta();
      }else{
        console.log("Foi atingida a quantidade máxima de erros :(");
        clearInterval(interval);
      }
    }else{
      apostaDnv = true;
    }

}, 4 * 1000);
